#include <gtest/gtest.h>
#include "../Logger/Logger.h"
#include <thread>
#include <cstring>

TEST(LoggerTest, Set_log_level)
{
    Logger *log = Logger::getInstance();
    ASSERT_FALSE(log->set_log_level(LOG_LEVEL_MIN - 1));
    ASSERT_FALSE(log->set_log_level(LOG_LEVEL_MAX + 1));
    ASSERT_TRUE(log->set_log_level(1));
    ASSERT_TRUE(log->set_log_level(2));
    ASSERT_TRUE(log->set_log_level(3));
    ASSERT_TRUE(log->set_log_level(4));
}

TEST(LoggerTest, basic_log)
{
    Logger *log = Logger::getInstance();
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_INFO, "info msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_ERROR, "error msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_WARNING, "warning msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_DEBUG, "debug msg to log %d", 1));
}

TEST(LoggerTest, extended_log)
{
    Logger *log = Logger::getInstance();
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "info msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "error msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_WARNING, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "warning msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "debug msg to log %d", 1));
}

TEST(LoggerTest, macro)
{
    Logger *log = Logger::getInstance();
    ASSERT_TRUE(log->log_info("info msg to log %d", 1));
    ASSERT_TRUE(log->log_error("error msg to log %d", 1));
    ASSERT_TRUE(log->log_warning("warning msg to log %d", 1));
    ASSERT_TRUE(log->log_debug("debug msg to log %d", 1));
}

TEST(LoggerTest, log_level_info)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_INFO);

    ASSERT_TRUE(log->extended_log(LOG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "info msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "error msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_WARNING, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "warning msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "debug msg to log %d", 1));

    ASSERT_TRUE(log->basic_log(LOG_LEVEL_INFO, "info msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_ERROR, "error msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_WARNING, "warning msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_DEBUG, "debug msg to log %d", 1));

    ASSERT_TRUE(log->log_info("info msg to log %d", 1));
    ASSERT_FALSE(log->log_error("error msg to log %d", 1));
    ASSERT_FALSE(log->log_warning("warning msg to log %d", 1));
    ASSERT_FALSE(log->log_debug("debug msg to log %d", 1));
}

TEST(LoggerTest, log_level_error)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_ERROR);

    ASSERT_TRUE(log->extended_log(LOG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "info msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "error msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_WARNING, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "warning msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "debug msg to log %d", 1));

    ASSERT_TRUE(log->basic_log(LOG_LEVEL_INFO, "info msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_ERROR, "error msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_WARNING, "warning msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_DEBUG, "debug msg to log %d", 1));

    ASSERT_TRUE(log->log_info("info msg to log %d", 1));
    ASSERT_TRUE(log->log_error("error msg to log %d", 1));
    ASSERT_FALSE(log->log_warning("warning msg to log %d", 1));
    ASSERT_FALSE(log->log_debug("debug msg to log %d", 1));
}

TEST(LoggerTest, log_level_warning)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_WARNING);

    ASSERT_TRUE(log->extended_log(LOG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "info msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "error msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_WARNING, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "warning msg to log %d", 1));
    ASSERT_FALSE(log->extended_log(LOG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "debug msg to log %d", 1));

    ASSERT_TRUE(log->basic_log(LOG_LEVEL_INFO, "info msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_ERROR, "error msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_WARNING, "warning msg to log %d", 1));
    ASSERT_FALSE(log->basic_log(LOG_LEVEL_DEBUG, "debug msg to log %d", 1));

    ASSERT_TRUE(log->log_info("info msg to log %d", 1));
    ASSERT_TRUE(log->log_error("error msg to log %d", 1));
    ASSERT_TRUE(log->log_warning("warning msg to log %d", 1));
    ASSERT_FALSE(log->log_debug("debug msg to log %d", 1));
}

TEST(LoggerTest, log_level_debug)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_DEBUG);

    ASSERT_TRUE(log->extended_log(LOG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "info msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "error msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_WARNING, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "warning msg to log %d", 1));
    ASSERT_TRUE(log->extended_log(LOG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, THREAD_ID, "debug msg to log %d", 1));

    ASSERT_TRUE(log->basic_log(LOG_LEVEL_INFO, "info msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_ERROR, "error msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_WARNING, "warning msg to log %d", 1));
    ASSERT_TRUE(log->basic_log(LOG_LEVEL_DEBUG, "debug msg to log %d", 1));

    ASSERT_TRUE(log->log_info("info msg to log %d", 1));
    ASSERT_TRUE(log->log_error("error msg to log %d", 1));
    ASSERT_TRUE(log->log_warning("warning msg to log %d", 1));
    ASSERT_TRUE(log->log_debug("debug msg to log %d", 1));
}

TEST(LoggerTest, LongLogAdd)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_DEBUG);
    char long_buffer[MAX_LOG_LEN + 1] = {'\0'};
    std::memset(long_buffer, '1', sizeof(char) * MAX_LOG_LEN + 1);
    ASSERT_TRUE(log->log_info(long_buffer));
}

TEST(LoggerTest, Bad_set_log_level)
{
    Logger *log = Logger::getInstance();
    log->set_log_level(0);
    char long_buffer[MAX_LOG_LEN + 1] = {'\0'};
    std::memset(long_buffer, '1', sizeof(char) * MAX_LOG_LEN + 1);
    ASSERT_TRUE(log->log_info(long_buffer));
}

TEST(LoggerTest, Performance_Test_1000_thread_10_loop_write_each_write_4_types)
{
#define THREAD_COUNT 1000
#define FOR_COUNT_THREAD 10
    Logger *log = Logger::getInstance();
    log->set_log_level(LOG_LEVEL_DEBUG);
    std::vector<std::thread> vectOfThread;
    for (int i = 0; i < THREAD_COUNT; i++)
    {
        vectOfThread.emplace_back([&, i] {
            int temp = i;
            for (size_t j = 0; j < FOR_COUNT_THREAD; j++)
            {
                log->log_info("THREAD -[%d] loop iterator-[%d] info msg to log ", temp, j);
                log->log_error("THREAD -[%d] loop iterator-[%d] error msg to log", temp, j);
                log->log_warning("THREAD -[%d] loop iterator-[%d] warning msg to log", temp, j);
                log->log_debug("THREAD -[%d] loop iterator-[%d] debug msg to log", temp, j);
            }
        });
    }
    for (auto &th : vectOfThread)
    {
        if (th.joinable())
            th.join();
    }
}