# ThreadSafeLoggerCPP

#### Builds Status
[![pipeline status](https://gitlab.com/wrable/threadsafeloggercpp/badges/master/pipeline.svg)](https://gitlab.com/wrable/threadsafeloggercpp/commits/master)

#### Test Coverage
[![coverage report](https://gitlab.com/wrable/threadsafeloggercpp/badges/master/coverage.svg)](https://gitlab.com/wrable/threadsafeloggercpp/-/commits/master)

### Getting Started

## Features

<!-- *   To działa -->

### Operating Systems

*   Linux 
*   Windows

### Compilers

*   [x] gcc 9.3+
*   [x] x86_64-w64-mingw32-g++-posix
*   [ ] i686-w64-mingw32-c++-posix

### Build Systems

*   [CMake](https://cmake.org/)

Happy logging!

## Todo
- [ ] RTC 0.1.10
- [x] Add Unit Tests (req_cover > 80%) 
- [x] clear main.cpp for cmake build 
- [ ] Example of usage
    - [ ] Add example.cpp in example folder    
- [x] Add pipeline badge
- [x] Test coverage calculation
- [x] Add test cover badge
- [ ] build package for linux (make install)
- [ ] Add licence 