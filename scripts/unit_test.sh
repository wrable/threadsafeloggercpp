mkdir build
cd build
cmake ..
make -j8
cd test
./LoggerTest --gtest_output="xml:report.xml"