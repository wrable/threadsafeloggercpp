#pragma once

#include <thread>
#include <mutex>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <unistd.h>
#include <cstring>
#include <cstdarg>

#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#endif

#ifndef LOGGER_FILE_NAME
#define LOGGER_FILE_NAME "UNDEF_LOG"
#endif

#ifndef LOGGER_LOG_HEADER_PROJECT_NAME
#define LOGGER_LOG_HEADER_PROJECT_NAME "UNDEF_PROJECT_NAME"
#endif

#ifndef LOGGER_LOG_HEADER_COMMIT
#define LOGGER_LOG_HEADER_COMMIT "UNDEF_COMMIT_ID"
#endif

#ifndef LOGGER_LOG_HEADER_VERSION
#define LOGGER_LOG_HEADER_VERSION "UNDEF_VERSION"
#endif

#ifndef MAX_LOG_LEN
#define MAX_LOG_LEN 2048
#endif

#define THREAD_ID std::this_thread::get_id()

#ifdef LOGGER_EXTENDED_LOG
#define log_info(msg, ...) extended_log(LOG_LEVEL_INFO,__FUNCTION__, __FILE__, __LINE__, THREAD_ID,msg, ##__VA_ARGS__)
#define log_warning(msg, ...) extended_log(LOG_LEVEL_WARNING,__FUNCTION__, __FILE__, __LINE__, THREAD_ID,msg, ##__VA_ARGS__)
#define log_error(msg, ...) extended_log(LOG_LEVEL_ERROR,__FUNCTION__, __FILE__, __LINE__, THREAD_ID,msg, ##__VA_ARGS__)
#define log_debug(msg, ...) extended_log(LOG_LEVEL_DEBUG,__FUNCTION__, __FILE__, __LINE__, THREAD_ID,msg, ##__VA_ARGS__)
#endif

#ifndef LOGGER_EXTENDED_LOG
#define log_info(msg, ...) basic_log(LOG_LEVEL_INFO,msg, ##__VA_ARGS__)
#define log_warning(msg, ...) basic_log(LOG_LEVEL_WARNING,msg, ##__VA_ARGS__)
#define log_error(msg, ...) basic_log(LOG_LEVEL_ERROR,msg, ##__VA_ARGS__)
#define log_debug(msg, ...) basic_log(LOG_LEVEL_DEBUG,msg, ##__VA_ARGS__)
#endif

#define LOG_LEVEL_MIN 1
#define LOG_LEVEL_MAX 4

#define LOG_LEVEL_INFO 1
#define LOG_LEVEL_ERROR 2
#define LOG_LEVEL_WARNING 3
#define LOG_LEVEL_DEBUG 4

#define LOG_INFO_TXT "   INFO  "
#define LOG_ERROR_TXT "  ERROR  "
#define LOG_WARNING_TXT " WARNING "
#define LOG_DEBUG_TXT "  DEBUG  "

class Logger
{
public:
    static Logger *getInstance();
    bool basic_log(std::uint8_t log_type,const char *const msg, ...);
    bool extended_log(std::uint8_t log_type,const std::string &function, const std::string &file, const int &line, const std::thread::id &thread_id,const char *const msg, ...);
    bool set_log_level(const std::uint8_t &set_log_level);
protected:
    bool _check_log_level(const std::uint8_t &check_level);
    bool _write_log(const std::string &type, const std::string &msg);
    bool _date_changed(const std::string &current_date);
    bool _write_to_file(const std::string &msg);
    std::string _get_log_type_text(std::uint8_t log_type);
    std::string _prepare_extended(const std::string &function, const std::string &file, const int &line, const std::thread::id &thread_id);
    std::string _prepare_header();
    std::string _process_va(const char *const msg, va_list &args);
    inline std::string _get_time();
    inline std::string _get_date();
private:
    Logger();
    ~Logger() = default;
    Logger(const Logger &) = delete;
    Logger &operator=(const Logger &) = delete;
    std::ofstream log_handler;
    std::string file_name;
    std::int8_t log_level;
    std::string header;
    std::string old_date;
    std::mutex ctxMutex;
};