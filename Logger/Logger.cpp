#include "Logger.h"
Logger::Logger()
{
    std::lock_guard<std::mutex> guard(ctxMutex);
    Logger::log_level = LOG_LEVEL_DEBUG;
    Logger::file_name = LOGGER_FILE_NAME;
    Logger::header = _prepare_header();
};
Logger *Logger::getInstance()
{
    static Logger instance;
    return &instance;
};
bool Logger::basic_log(std::uint8_t log_type, const char *const msg, ...)
{
    bool ret_val = false;
    va_list args;
    va_start(args, msg);
    if (_check_log_level(log_type))
    {
        ret_val = _write_log(_get_log_type_text(log_type), _process_va(msg, args));
    }
    va_end(args);
    return ret_val;
};
bool Logger::extended_log(std::uint8_t log_type, const std::string &function, const std::string &file, const int &line, const std::thread::id &thread_id, const char *const msg, ...)
{
    bool ret_val = false;
    va_list args;
    va_start(args, msg);
    if (_check_log_level(log_type))
    {
        ret_val = _write_log(_get_log_type_text(log_type), (_process_va(msg, args) + _prepare_extended(function, file, line, thread_id)));
    }
    va_end(args);
    return ret_val;
};
bool Logger::set_log_level(const std::uint8_t &set_log_level)
{
    if (set_log_level <= LOG_LEVEL_MAX && set_log_level >= LOG_LEVEL_MIN)
    {
        std::lock_guard<std::mutex> guard(ctxMutex);
        Logger::log_level = set_log_level;
        return true;
    }
    return false;
};
bool Logger::_check_log_level(const std::uint8_t &check_level)
{
    if (Logger::log_level >= check_level)
    {
        return true;
    }
    return false;
};
std::string Logger::_get_log_type_text(std::uint8_t log_type)
{
    std::string buffer;
    switch (log_type)
    {
    case LOG_LEVEL_INFO:
        buffer = LOG_INFO_TXT;
        break;
    case LOG_LEVEL_WARNING:
        buffer = LOG_WARNING_TXT;
        break;
    case LOG_LEVEL_ERROR:
        buffer = LOG_ERROR_TXT;
        break;
    case LOG_LEVEL_DEBUG:
        buffer = LOG_DEBUG_TXT;
        break;
    }
    return buffer;
};
bool Logger::_write_log(const std::string &type, const std::string &msg)
{
    std::string date = _get_date();
    if (Logger::_date_changed(date))
    {
        _write_to_file(header);
    }
    std::stringstream buffer = std::stringstream{};
    buffer << "|" << date << "| (" << Logger::_get_time() << ")"
           << " {" << type << "}"
           << " [" << msg << "]" << std::endl;
    return _write_to_file(buffer.str());
};
std::string Logger::_prepare_header()
{
    char hostname[100] = {'\0'};
#ifdef WIN32
    DWORD size = sizeof(hostname);
    GetComputerNameA(hostname, &size);
#else
    gethostname(hostname, 100);
#endif
    std::stringstream buffer = std::stringstream{};
    buffer << std::endl
           << "### " << LOGGER_LOG_HEADER_PROJECT_NAME << ",version:" << LOGGER_LOG_HEADER_VERSION
           << ",commit_id:" << LOGGER_LOG_HEADER_COMMIT << ",host:" << hostname << " ###" << std::endl
           << std::endl;
    return buffer.str();
};
std::string Logger::_prepare_extended(const std::string &function, const std::string &file, const int &line, const std::thread::id &thread_id)
{
    std::stringstream buffer = std::stringstream{};
    buffer << " *"
           << "Func:" << function << " File:"
           << file << " Line:" << line << " ThreadID:" << thread_id << "*";
    return buffer.str();
};
std::string Logger::_process_va(const char *const msg, va_list &args)
{
    char buffer[MAX_LOG_LEN] = {'\0'};
    int len = vsnprintf(buffer, MAX_LOG_LEN, msg, args);

    if (len >= MAX_LOG_LEN)
    {
        std::memset(buffer, '\0', sizeof(char)*MAX_LOG_LEN);
        snprintf(buffer,MAX_LOG_LEN,"##LOGGER-ERROR## Too long msg to write! - set biger buffer #define MAX_LON_LEN %d",MAX_LOG_LEN);
    }
    return buffer;
};
bool Logger::_write_to_file(const std::string &msg)
{
    std::lock_guard<std::mutex> guard(ctxMutex);
    std::string date = _get_date();
    Logger::log_handler.open(Logger::file_name + date, std::ios::out | std::ios::app);
    if (Logger::log_handler.is_open())
    {
        Logger::log_handler << msg;
        Logger::log_handler.close();
        return true;
    }
    return false;
};
bool Logger::_date_changed(const std::string &current_date)
{
    if (current_date != Logger::old_date)
    {
        Logger::old_date = current_date;
        return true;
    }
    return false;
};
inline std::string Logger::_get_time()
{
    std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
    std::time_t tt = std::chrono::system_clock::to_time_t(tp);
    std::tm gmt{};
#ifdef WIN32
    gmtime_s(&gmt, &tt);
#else
    gmtime_r(&tt, &gmt);
#endif
    std::chrono::duration<double> fractional_seconds =
        (tp - std::chrono::system_clock::from_time_t(tt)) + std::chrono::seconds(gmt.tm_sec);
    std::string buffer("hr:mn:sc.xxxxxx");
    sprintf(&buffer.front(), "GMT %02d:%02d:%09.6f", gmt.tm_hour, gmt.tm_min,
            fractional_seconds.count());
    return buffer;
};
inline std::string Logger::_get_date()
{
    std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
    std::time_t tt = std::chrono::system_clock::to_time_t(tp);
    std::tm gmt{};
#ifdef WIN32
    gmtime_s(&gmt, &tt);
#else
    gmtime_r(&tt, &gmt);
#endif
    std::string buffer("year-mo-dy");
    sprintf(&buffer.front(), "%04d-%02d-%02d", gmt.tm_year + 1900, gmt.tm_mon + 1,
            gmt.tm_mday);
    return buffer;
};